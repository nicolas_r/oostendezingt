import Vue from 'vue';
import Router from 'vue-router';
import SongTeksten from '@/components/SongTeksten';
import OostendeZingt from '@/components/OostendeZingt';
import OostendeZingtResults from '@/components/OostendeZingtResults';
import VoteConfirmed from '@/components/VoteConfirmed';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'OostendeZingt',
      component: OostendeZingt,
    },
    {
        path: '/resultaat',
        name: 'resultaat',
        component: OostendeZingtResults,
    },
    {
      path: '/songteksten/:id?', // Optional
      name: 'SongTeksten',
      component: SongTeksten,
      props: true,
    },
    {
      path: '/confirm',
      name: 'VoteConfirmed',
      component: VoteConfirmed,
    },
  ],
  // watch: {
  //   '$route': 'fetchData'
  // },
  // methods: {
  //   fetchData() {
  //     console.log("fetching");
  //   }
  // }
});
