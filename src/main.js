import 'bootstrap/dist/css/bootstrap.min.css'
import Vue from 'vue';
import * as uiv from 'uiv';
import App from './App';
import router from './router';

Vue.use(uiv);
Vue.config.productionTip = false;

import * as configJson from './config.json';
const server = _.findKey(configJson, function(o) { return o.hosts.indexOf(window.location.host) !== -1 });
Vue.prototype.$access = {
  token: '',
  key: 'acrobat',
  url: configJson[server].api.url,
};

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
